# Proton Launcher

Launch multiple applications with your Proton/Steam game.

## How to install
Copy the release zip into your **$HOME/bin/** directory, don't worrie about the config.json, some games has a different working directory, it will generate a new one for that game.

## How to run:
You might need to know what version of proton are you using, usualy are store in, **$HOME/.steam/steam/steamapps/common/'Proton 5.x'** or in your custom library directory.

Go to your game **Properties**, into the **General** tab, change the **Launch Options** and play the game:

![Poton_Launcher_steam](./Proton_Launcher_Steam.png "Proton Launcher Steam")

 $HOME/.steam/steam/steamapps/common/'Proton 5.0'/proton waitforexitandrun ~/bin/proton_launcher/Proton_Launcher.exe; echo %command%

![Proton-Launcher](./Proton_launcher.png "Proton Launcher")
![Proton-Launcher](./Proton_launcher_rf2.png "Proton Launcher rFactor 2 tab")
![Proton-Launcher](./Proton_launcher_Rf2_app.png "Proton Launcher rFactor 2 Application")

If your game is in hide folder, you can enable show hidden files with winecfg, or user the entry box next to "Add Aplication" button and paste the path eg.:

 /home/user/.steam/steam/steamapps/common/game/game.exe

# TODO:

# Know issues:
 - **Proton 6 and Proton-GE**: it maight require to use glibc 2.28 or higher.

 - **Proton 5.13**: you need a special command launcher

 ~/.local/share/Steam/steamapps/common/SteamLinuxRuntime_soldier/_v2-entry-point --verb=waitforexitandrun -- $HOME/.steam/steam/steamapps/common/'Proton 5.13'/proton waitforexitandrun ~/bin/proton_launcher/Proton_Launcher.exe; echo %command%

# Atributes
Icon : Made by **"Freepik"** from www.flaticon.com
