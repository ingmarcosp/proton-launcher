from tkinter import *
from tkinter import ttk, filedialog, StringVar
from pathlib import Path
from icoextract import IconExtractor
from PIL import ImageTk, Image
from io import BytesIO
import json
import subprocess
# import pdb


class ProtonLauncherGui(ttk.Frame):
    """GUI for Proton Luncher """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.parent.title('Proton Launcher')
        self.game_name = StringVar()
        self.create_widgets()
        self.arrange()
        self.load_config()

    def create_widgets(self):
        """ To be a bit clear:
        e: entry
        b: button
        nb: notebook
        """
        self.intro_label = ttk.Label(self, text='Testing Proton Launcher', anchor='center')
        self.e_game = ttk.Entry(self, textvariable=self.game_name)
        self.b_add_game = ttk.Button(self, text='Add Game', command=self.add_game_tab)
        self.nb_games = Notebook(self)
        self.b_save_config = ttk.Button(self, text='Save Configuration', command=self.save_config, state='disable')

    def arrange(self):
        """        """
        self.parent.rowconfigure(0, weight=1)
        self.parent.columnconfigure(0, weight=1)
        self.grid(column=0, row=0, sticky=(N, S, E, W))
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=3)
        self.rowconfigure(2, weight=1)
        # column: 0
        self.intro_label.grid(column=0, row=0, columnspan=4,
                              sticky=(N, S, E, W), pady=5)
        self.b_add_game.grid(column=0, row=1, padx=3, pady=5)
        self.nb_games.grid(column=0, row=2, columnspan=4,
                          sticky=(N, S, E, W), pady=5, padx=3)
        # column: 1
        self.e_game.grid(column=1, row=1, columnspan=3, sticky=(E, W), padx=5)
        # column: 3
        self.b_save_config.grid(column=3, row=3, pady=5, padx=5)

    # Tab managment
    def add_game_tab(self, name=None):
        """factory getting the name from the entry box self.game_name
        or by argument name"""
        name = self.game_name.get() or name
        if name and not self.nb_games.exist(name):
            new_game = Game(self.nb_games)
            new_game.name = name
            self.nb_games.add(new_game, text=name)
            self.e_game.delete(0, 'end')
            self.b_save_config.configure(state='normal')

    def forget_tab(self):
        """Delete the tab selected, I sould distroy it."""
        frame = self.nb_games.select()
        frame = self.apps_args.pop(self.nametowidget(frame))
        self.nb_games.forget(frame)
        frame.destroy()
        if not self.nb_games.tabs():
            self.b_add_application.configure(state='disabled')
            self.e_app.configure(state='disabled')

    # Configuration
    def save_config(self):
        """        """
        PROTON_LAUNCHER = self.nb_games.settings()
        with open('config.json', 'w') as config_file:
            json.dump(PROTON_LAUNCHER, config_file)

    def load_config(self):
        """ """
        if not Path('config.json').exists():
            self.save_config()
        with open('config.json', 'r') as config_file:
            PROTON_LAUNCHER = json.load(config_file)
        if PROTON_LAUNCHER:
            for tab in PROTON_LAUNCHER:
                self.add_game_tab(name=tab)
                game = self.nb_games.games[-1]
                game.load_config(PROTON_LAUNCHER[tab])


class Notebook(ttk.Notebook):
    """ """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.games = []

    def add(self, game, text=None):
        self.games.append(game)
        super().add(game, text=text)

    def settings(self):
        settings = {}
        for game in self.games:
            settings[game.name] = game.settings()
        return settings

    def exist(self, game_name):
        for game in self.games:
            if game.name == game_name:
                return True


class Game(ttk.Frame):
    """ """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.name = None
        self.apps = []
        self.app_path = StringVar()
        self.row = 0
        self.create_widgets()
        self.arrange()

    def create_widgets(self):
        self.b_add_app = ttk.Button(self, text='Add Application', command=self.add_app)
        self.e_app_path = ttk.Entry(self, textvariable=self.app_path)
        self.b_delete_game = ttk.Button(self, text='Delete Game', command=self.destroy)
        self.separator = ttk.Separator(self, orient=HORIZONTAL)

    def arrange(self):
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=3)
        self.b_add_app.grid(column=0, row=0, padx=5, pady=5)
        self.e_app_path.grid(column=1, row=0, sticky=(E, W), pady=5)
        self.b_delete_game.grid(column=2, row=0, columnspan=3, padx=5, pady=5)
        self.separator.grid(column=0, row=1, columnspan=4, sticky=(E, W), pady=5)
        self.row += 2

    def add_app(self):
        app_path = self.app_path.get() or filedialog.askopenfilename()
        if app_path:
            if app_path[0] == '/':
                app_path = 'z:' + app_path
            app = App(self, path=app_path)
            self.apps.append(app)
            self.e_app_path.delete(0, 'end')
            self.row += 1

    def delete_app(self, app):
        self.apps.remove(app)

    def settings(self):
        game = {}
        for app in self.apps:
            path, args = app.settings()
            game[path] = args
        return game

    def load_config(self, apps):
        for app_path in apps:
            self.app_path.set(app_path)
            self.add_app()
            self.apps[-1].load_config(apps[app_path])


class App(ttk.Frame):
    """ """
    def __init__(self, parent=None, path=None):
        super().__init__(parent)
        self.parent = parent
        self.path = path
        self.args = StringVar()
        self.create_widgets()
        self.arrange()

    def create_widgets(self):
        name = self.path.split('/')[-1]
        self.b_app = ttk.Button(self, text=name, command=self.open_app)
        self.entry = ttk.Entry(self, textvariable=self.args)
        self.b_close = ttk.Button(self, text="X", command=self.delete_app)
        self.l_name = ttk.Label(self, text=name)
        # TODO for future release
        # img = Image.open("./proton.png").resize((20, 20))
        # tkimg = ImageTk.PhotoImage(img)
        # self.b_close.configure(image=tkimg)

    def arrange(self):
        self.grid(column=0, row=self.parent.row, columnspan=3, pady=3, padx=3, sticky=(N, S, E, W))
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=3)
        if self.get_icon():
            self.b_app.configure(image=self.image)
            self.b_app.grid(column=0, row=0, rowspan=2, pady=3, padx=3)
            self.l_name.grid(column=1, row=0, pady=3, padx=3)
            self.b_close.grid(column=2, row=0, pady=3, padx=3)
            self.entry.grid(column=1, row=1, columnspan=2, pady=3, padx=3, sticky=(E, W))
        else:
            self.b_app.grid(column=0, row=0, pady=3, padx=3)
            self.entry.grid(column=1, row=0, pady=3, padx=3, sticky=(E, W))
            self.b_close.grid(column=2, row=0, pady=3, padx=3)

    def open_app(self):
        subprocess.Popen(self.path + self.args.get())

    def get_icon(self):
        try:
            # this fails on linux/python execution but
            # should fine on wine/proton binary
            # path = self.path.replace("z:", "")
            extractor = IconExtractor(self.path)
            icon = extractor.get_icon()
            img = Image.open(BytesIO(icon.getvalue())).resize((40, 40))
            tkimg = ImageTk.PhotoImage(img)
            self.image = tkimg
            return True
        except Exception as e:
            return False

    def delete_app(self):
        self.parent.delete_app(self)
        self.destroy()

    def settings(self):
        return self.path, self.args.get()

    def load_config(self, args):
        self.args.set(args)


if __name__ == '__main__':
    parent = Tk()
    gui = ProtonLauncherGui(parent)
    gui.mainloop()
